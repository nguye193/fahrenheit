package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelcilusRegular() {
		Fahrenheit fa = new Fahrenheit();
		double fahren = Fahrenheit.fromCelcilus(32);
		assertTrue("The time provided does not match the result", fahren == 89);
	}
	
	@Test
	public void testFromCelcilusExceptional() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testFromCelcilusBoundaryIn() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testFromCelcilusBoundaryOut() {
		fail("Not yet implemented");
	}

}
